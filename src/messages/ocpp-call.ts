
import { AbstractOcppMessage } from './abstract-ocpp-message';
import { OcppCommand } from '../enums/ocpp-commands'
import { OcppMessageType } from './ocpp-message-type';

export class OcppCall<Tpayload> extends AbstractOcppMessage {
  command: OcppCommand;
  payload: Tpayload;

  constructor(data: {
    id: string,
    command: OcppCommand,
    payload: Tpayload
  }) {
    super(data.id);
    this.command = data.command;
    this.payload = data.payload;
  }

  type(): OcppMessageType {
    return OcppMessageType.Request;
  }

  toJson(): [OcppMessageType, string, string, Tpayload] {
    return [this.type(), this.id, this.command, this.payload];
  }
}
