export enum OcppMessageType {
    Request = 2,
    Result = 3,
    Error = 4
}