export enum OcppErrorCode {
	NotImplemented = 'NotImplemented',
	NotSupported = 'NotSupported',
	InternalError = 'InternalError',
	ProtocolError = 'ProtocolError',
	SecurityError = 'SecurityError',
	FormationViolation = 'FormationViolation',
	PropertyConstraintViolation = 'PropertyConstraintViolation',
	OccurenceConstraintViolation = 'OccurenceConstraintViolation',
	TypeConstraintViolation = 'TypeConstraintViolation',
	GenericError = 'GenericError'
}

export enum OcppErrorMessage {
	GenericError = '',
	NotImplemented = 'Requested Action is not known by receiver',
	NotSupported = 'Requested Action is recognized but not supported by the receiver',
	InternalError = 'An internal error occurred and the receiver was not able to process the requested Action successfully',
	ProtocolError = 'Payload for Action is incomplete',
	SecurityError = 'During the processing of Action a security issue occurred preventing receiver from completing the Action successfully',
	FormationViolation = 'Payload for Action is syntactically incorrect or not conform the PDU structure for Action',
	PropertyConstraintViolation = 'Payload is syntactically correct but at least one field contains an invalid value',
	OccurenceConstraintViolation = 'Payload for Action is syntactically correct but at least one of the fields violates occurence constraints',
	TypeConstraintViolation = 'Payload for Action is syntactically correct but a least one of the fields violates data type constraints'
}