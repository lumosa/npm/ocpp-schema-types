/**/

export interface DataTransferResponse {
  status: DataTransferStatus;
  data?: string;
}

export enum DataTransferStatus {
  Accepted = "Accepted",
  Rejected = "Rejected",
  UnknownMessageId = "UnknownMessageId",
  UnknownVendorId = "UnknownVendorId"
}
