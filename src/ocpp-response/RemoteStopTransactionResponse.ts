/**/

export interface RemoteStopTransactionResponse {
  status: RemoteStopTransactionStatus;
}

export enum RemoteStopTransactionStatus {
  Accepted = "Accepted",
  Rejected = "Rejected"
}
