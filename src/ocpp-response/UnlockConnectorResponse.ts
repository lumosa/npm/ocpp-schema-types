/**/

export interface UnlockConnectorResponse {
  status: UnlockConnectorStatus;
}

export enum UnlockConnectorStatus {
  Unlocked = "Unlocked",
  UnlockFailed = "UnlockFailed",
  NotSupported = "NotSupported"
}
