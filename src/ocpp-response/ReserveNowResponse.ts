/**/

export interface ReserveNowResponse {
  status: ReserveNowStatus;
}

export enum ReserveNowStatus {
  Accepted = "Accepted",
  Faulted = "Faulted",
  Occupied = "Occupied",
  Rejected = "Rejected",
  Unavailable = "Unavailable"
}
