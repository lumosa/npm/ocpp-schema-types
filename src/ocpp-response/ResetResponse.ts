/**/

export interface ResetResponse {
  status: ResetStatus;
}

export enum ResetStatus {
  Accepted = "Accepted",
  Rejected = "Rejected"
}
