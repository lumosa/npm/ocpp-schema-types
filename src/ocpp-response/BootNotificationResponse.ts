/**/

export interface BootNotificationResponse {
  status: BootNotificationStatus;
  currentTime: string;
  interval: number;
}

export enum BootNotificationStatus {
  Accepted = "Accepted",
  Pending = "Pending",
  Rejected = "Rejected"
}
