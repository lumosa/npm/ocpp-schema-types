/**/

export interface TriggerMessageResponse {
  status: TriggerMessageStatus;
}

export enum TriggerMessageStatus {
  Accepted = "Accepted",
  Rejected = "Rejected",
  NotImplemented = "NotImplemented"
}
