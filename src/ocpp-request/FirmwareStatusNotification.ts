/**/

export interface FirmwareStatusNotificationRequest {
  status: FirmwareStatusNotificationStatus;
}

export enum FirmwareStatusNotificationStatus {
  Downloaded = "Downloaded",
  DownloadFailed = "DownloadFailed",
  Downloading = "Downloading",
  Idle = "Idle",
  InstallationFailed = "InstallationFailed",
  Installing = "Installing",
  Installed = "Installed"
}
