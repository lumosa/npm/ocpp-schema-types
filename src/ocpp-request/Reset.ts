/**/

export interface ResetRequest {
  type: ResetType;
}

export enum ResetType {
  Hard = "Hard",
  Soft = "Soft"
}
