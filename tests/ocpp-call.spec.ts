import { OcppCall, OcppMessageType } from '../src/messages/index';
import { OcppCommand } from '../src/enums/index';

describe('ocpp call', () => {
  it('must be the correct type', () => {
    const v = new OcppCall({
      id: "0",
      command: OcppCommand.Authorize,
      payload: {}
    });
    expect(v.type()).toBe(OcppMessageType.Request);
  });

  it('must return correct payload if provided', () => {
    const payload = { a: 'dummy', b: 4, c: true, d: {}, e: [1, 2, 3] };
    const v = new OcppCall({
      id: "0",
      command: OcppCommand.Authorize,
      payload
    });
    expect(v.payload).toEqual(payload);
  });

  it('must return correct ocpp result json', () => {
    const id = '1213224';
    const command = OcppCommand.Authorize;
    const payload = { a: 'dummy' };
    const v = new OcppCall({
      id,
      command,
      payload
    });
    expect(v.toJson()).toEqual([OcppMessageType.Request, id, command, payload]);
  });

  it('must return correct stringified data', () => {
    const id = '1213224';
    const command = OcppCommand.Authorize;
    const payload = { a: 'dummy' };
    const v = new OcppCall({
      id,
      command,
      payload
    });
    expect(v.toString()).toBe(JSON.stringify([OcppMessageType.Request, id, command, payload]));
  });
});
