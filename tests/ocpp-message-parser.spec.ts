import { OcppCommand, OcppErrorCode } from "../src/enums";
import { OcppMessageParser, OcppMessageType } from "../src/messages";

describe('ocpp message parser', () => {

    describe('for ocpp call error', () => {
        it('should work as expected', () => {
            const data = [OcppMessageType.Error, '1234', OcppErrorCode.FormationViolation, 'dummy', {}];
            const value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value.toJson()).toEqual(data);
        });

        xit('should return null if error code not valid', () => {
            const data = [OcppMessageType.Error, '1234', 'strange-error-code', 'dummy', {}];
            const value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();
        });

        it('should return null if not json data', () => {
            const data = 'invalid-json';
            const value = OcppMessageParser.parse(data);
            expect(value).toBeNull();
        });

        it('should return null if type not call error', () => {
            const data = [OcppMessageType.Request, '1234', OcppErrorCode.FormationViolation, 'dummy', {}];
            const value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();
        });

        it('should return null if data length not 5', () => {
            let data = [
            OcppMessageType.Error,
            '1234',
            OcppErrorCode.FormationViolation,
            'dummy',
            {},
            true,
            8
            ];
            let value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [OcppMessageType.Error, '1234', OcppErrorCode.FormationViolation];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();
        });

        it('should return null when id value that is not string', () => {
            let data = [OcppMessageType.Error, true, OcppErrorCode.FormationViolation, 'dummy', {}];
            let value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [OcppMessageType.Error, 3242534, OcppErrorCode.FormationViolation, 'dummy', {}];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [OcppMessageType.Error, {}, OcppErrorCode.FormationViolation, 'dummy', {}];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [OcppMessageType.Error, {}, OcppErrorCode.FormationViolation, 'dummy', {}];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();
        });

        it('should return null when error message that is not string', () => {
            let data = [OcppMessageType.Error, '1234', OcppErrorCode.FormationViolation, true, {}];
            let value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [OcppMessageType.Error, '1234', OcppErrorCode.FormationViolation, 11111, {}];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [OcppMessageType.Error, '1234', OcppErrorCode.FormationViolation, {}, {}];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [OcppMessageType.Error, '1234', OcppErrorCode.FormationViolation, [], {}];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();
        });
    });

    describe('for ocpp call result', () => {
        it('should works as expected', () => {
            const data = [OcppMessageType.Result, '1234', {}];
            const value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value.toJson()).toEqual(data);
        });

        it('should return nullnif not json data', () => {
            const data = 'invalid-json';
            const value = OcppMessageParser.parse(data);
            expect(value).toBeNull();
        });

        it('should return null if type not call result', () => {
            const data = [OcppMessageType.Error, 'dummy', {}];
            const value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();
        });

        it('should return null if data length not 3', () => {
            const data1 = [OcppMessageType.Result, 'dummy'];
            const data2 = [OcppMessageType.Result, 'dummy', {}, {}];
            const value1 = OcppMessageParser.parse(JSON.stringify(data1));
            expect(value1).toBeNull();

            const value2 = OcppMessageParser.parse(JSON.stringify(data2));
            expect(value2).toBeNull();
        });

        it('should return null when id value that is not string', () => {
            let data = [OcppMessageType.Result, true, {}];
            let value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [OcppMessageType.Result, 12345, {}];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [OcppMessageType.Result, {}, {}];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [OcppMessageType.Result, [], {}];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();
        });
    });

    describe('for ocpp call', () => {
        it('should work as expected', () => {
            const data = [2, '23124', OcppCommand.Authorize, {}];
            const newValue = OcppMessageParser.parse(JSON.stringify(data));
            expect(newValue.toJson()).toEqual(data);
        });

        it('should return null when trying to parse incorrect message type', () => {
            const s = JSON.stringify([OcppMessageType.Error, '1324', OcppCommand.Authorize, {}]);
            const ok = OcppMessageParser.parse(s);
            console.log(s, ok)
            expect(ok).toBeNull();
        });

        it('should return null when parsing dummy string', () => {
            const ok = OcppMessageParser.parse('dummy');
            expect(ok).toBeNull();
        });

        it('should return null when parsing message with length that is not 4', () => {
            let data = JSON.stringify([OcppMessageType.Request, '1324', 'a', 3, true]);
            let ok = OcppMessageParser.parse(data);
            expect(ok).toBeNull();

            data = JSON.stringify([OcppMessageType.Request, '1324', 'a']);
            ok = OcppMessageParser.parse(data);
            expect(ok).toBeNull();
        });

        it('should return null when id that is not string', () => {
            let data = [2, true, OcppCommand.Authorize, {}];
            let value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [2, 124, OcppCommand.Authorize, {}];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [2, {}, OcppCommand.Authorize, {}];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [2, [], OcppCommand.Authorize, {}];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();
        });

       xit('should return null when command that is not string', () => {
            let data = [2, '23124', true, {}];
            let value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [2, '23124', {}, {}];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [2, '23124', [], {}];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();

            data = [2, '23124', 1234, {}];
            value = OcppMessageParser.parse(JSON.stringify(data));
            expect(value).toBeNull();
        });

        xit('should return null when command is not a valid command type', () => {
            let data = [2, '23124', 'invalid-command-type', {}];
            let value = OcppMessageParser.parse(JSON.stringify(data));
            console.log(value);
            expect(value).toBeNull();
        });
    });

});