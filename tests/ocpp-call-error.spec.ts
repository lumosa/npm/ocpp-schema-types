import { OcppErrorCode, OcppErrorMessage } from '../src/enums';
import { OcppCallError, OcppMessageType } from '../src/messages';


describe('ocpp call error', () => {
  it('must be the correct type', () => {
    const v = new OcppCallError({
      id: "0",
      code: OcppErrorCode.FormationViolation
    });
    expect(v.type()).toBe(OcppMessageType.Error);
  });

  it('should return empty details if not provided', () => {
    const v = new OcppCallError({
      id: "0",
      code: OcppErrorCode.FormationViolation
    });
    expect(v.details).toEqual({});
  });

  it('must return correct payload if provided', () => {
    const details = { a: 'dummy', b: 4, c: true, d: {}, e: [1, 2, 3] };
    const v = new OcppCallError({
      id: "242135",
      code: OcppErrorCode.FormationViolation,
      details
    });
    expect(v.details).toEqual(details);
  });

  it('must return correct ocpp result json', () => {
    const id = '1213224';
    const code = OcppErrorCode.FormationViolation;
    const message = OcppErrorMessage.FormationViolation;
    const details = { a: 'dummy' };
    const v = new OcppCallError({
      id: id,
      code,
      details,
      message
    });
    expect(v.toJson()).toEqual([OcppMessageType.Error, id, code, message, details]);
  });

  it('must return correct stringified data', () => {
    const id = '1213224';
    const code = OcppErrorCode.NotSupported;
    const message = OcppErrorMessage.FormationViolation;
    const details = { a: 'dummy' };
    const v = new OcppCallError({
      id: id,
      code,
      details,
      message
    });
    expect(v.toString()).toBe(
      JSON.stringify([OcppMessageType.Error, id, code, message, details])
    );
  });

});
