# ocpp-shema-types

A project which uses official ocpp schema data (provided by OCA) to generate typescript type definitions for request and responses of each ocpp command.

Use type to enforce strong-typing in your code base or assure same data type across multiple projects.

Schemas can be used for validation using npm package ajv: <https://www.npmjs.com/package/ajv>.

## schema validation example

```typescript
  
    import Ajv2020 from 'ajv/dist/2020';
    import addFormats from "ajv-formats"

    import * as schema from '@lumosa/ocpp-schema-types/schemas/BootNotification.json';
    import { BootNotificationRequest } from '@lumosa/ocpp-schema-types/types/BootNotification';
    
    ...
    
    private _validator: Ajv2020;
    get validator(): Ajv2020 {
        if (this._validator) return this._validator;
        // Create validator
        const _validator = new Ajv2020();
        addFormats(_validator)
        return _validator;
    }
    ...
    
    const ocppRequest = [2, '1234566', 'BootNotification', {  chargePointVendor: 'a-dummy-vendor', chargePointModel: 'a-dummy-model'}];
    const payload: BootNotificationRequest = ocppRequest[3];
    const isSchemaValid = await this.validator.validate(schema, payload);
```

## strong typing example

```typescript
    import { StatusNotificationRequest } from '@lumosa/ocpp-schema-types/types/StatusNotification';
    import { StatusNotificationResponse } from '@lumosa/ocpp-schema-types/types/StatusNotificationResponse';
    
    ...
    
    const requestPayload: StatusNotificationRequest = {
        connectorId: 1,
        status: 'Available',
        error: 'NoError'
    };
    
    ...
    
    const responsePayload: StatusNotificationResponse = {};
```
